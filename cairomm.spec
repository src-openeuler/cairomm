%global apiver 1.0

%global cairo_version 1.10.0
%global libsigc_version 2.5.1

Name:           cairomm
Version:        1.14.5
Release:        1
Summary:        C++ API for the cairo graphics library
License:        LGPLv2+
URL:            http://www.cairographics.org
Source:         http://www.cairographics.org/releases/%{name}-%{version}.tar.xz

BuildRequires:  cairo-devel >= %{cairo_version}
BuildRequires:  libsigc++20-devel >= %{libsigc_version}
BuildRequires:  perl, perl(Getopt::Long), pkgconfig, gcc-g++
BuildRequires:  meson doxygen graphviz libxslt pkgconfig(mm-common-libstdc++)
BuildRequires:  boost-devel


Requires:       cairo%{?_isa} >= %{cairo_version}
Requires:       libsigc++20%{?_isa} >= %{libsigc_version}

%description
Cairo graphics library API, provides c++ developers cairo interfaces including
of Standard Template Library.

%package        devel
Summary:        Header files when using %{name} to develop program
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
Cairo graphics library API, provides c++ developers cairo interfaces including
of Standard Template Library.
This package contains the libraries and header files needed for
developing %{name} applications.

%package_help

%prep
%autosetup -p1

%build
%meson \
  -Dmaintainer-mode=%{?with_maintainer_mode:true}%{?!with_maintainer_mode:false} \
  -Dbuild-documentation=true \
  -Dbuild-examples=false \
  -Dbuild-tests=true \
  -Dboost-shared=true \
  -Dwarnings=max
%meson_build


%install
%meson_install

install -t %{buildroot}%{_docdir}/cairomm-%{apiver} -m 0644 -p \
    ChangeLog NEWS README.md
cp -rp examples %{buildroot}%{_docdir}/cairomm-%{apiver}/

%delete_la

%check
%meson_test

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license COPYING
%{_libdir}/lib*.so.*

%files devel
%doc ChangeLog
%{_includedir}/%{name}-%{apiver}
%{_libdir}/*.so
%{_libdir}/pkgconfig/*
%{_libdir}/%{name}-%{apiver}

%files help
%doc %{_datadir}/doc/%{name}-%{apiver}/
%doc %{_datadir}/devhelp/

%changelog
* Fri Dec 29 2023 Paul Thomas <paulthomas100199@gmail.com> - 1.14.5-1
- update to version 1.14.5

* Thu Sep 07 2023 chenchen <chen_aka_jan@163.com> - 1.14.4-1
- Upgrade to version 1.14.4

* Mon May 31 2021 baizhonggui <baizhonggui@huawei.com> - 1.12.0-11
- Fix building error: C++ compiler cannot create executables
- Add gcc-g++ in BuildRequires

* Sun Dec 1 2019  jiaxiya <jiaxiyajiaxiya@168.com> - 1.12.0-10
- Package init
